import {useState} from 'react'

export const useHighlights = (initialState) => {
  const [state, setState] = useState(initialState)
  const addHighlight = (highlight) => {
    setState([
      ...state,
      highlight,
    ])
  }

  const actions = {addHighlight}

  return [state, actions]
}
