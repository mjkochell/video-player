export const formatTime = (time: number) => {
  const minutes = Math.floor(time / 60)
  const seconds = Math.floor(time - minutes * 60)
  let secondsString = `${seconds}`
  if (seconds < 10) {
    secondsString = `0${seconds}`
  }

  return `${minutes}:${secondsString}`
}
