import React from 'react'

import VideoPlayer from './video-player'

class VideoPlayerContainer extends React.PureComponent {
  render() {
    return (
      <VideoPlayer />
    )
  }
}

export default VideoPlayerContainer
