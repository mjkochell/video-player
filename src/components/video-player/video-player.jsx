import React from 'react'
import { Player, ControlBar, ProgressControl, SeekBar } from 'video-react'
import 'video-react/dist/video-react.css'
import './styles.scss'

class VideoPlayer extends React.PureComponent {
  render() {
    return (
      <Player>
        <source src="https://media.w3.org/2010/05/sintel/trailer_hd.mp4" />

        <ControlBar>
          <ProgressControl
            key="progress-control"
            order={6}
            className='progress-bar'
          />
          <SeekBar />
        </ControlBar>
      </Player>
    )
  }
}

export default VideoPlayer
