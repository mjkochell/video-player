import React from 'react'
import PT from 'prop-types'
import {Popover as BootstrapPopover, OverlayTrigger} from 'react-bootstrap'

class Popover extends React.PureComponent {
  static propTypes = {
    renderRoot: PT.func.isRequired,
    renderPopoverBody: PT.func.isRequired,
    popoverTitle: PT.string.isRequired,
    placement: PT.string,
    trigger: PT.oneOfType([PT.string, PT.array]),
  }

  overlayRef = React.createRef()

  open = (e) => {
    e.stopPropagation()
  }

  hide = () => {
    this.overlayRef.current.hide()
  }

  render() {
    const {
      renderRoot,
      renderPopoverBody,
      popoverTitle,
      placement = 'top',
      trigger = 'click',
    } = this.props
    const popoverEventHandlers = {
      openPopover: this.open,
      hidePopover: this.hide,
    }

    const popoverElement = (
      <BootstrapPopover title={popoverTitle} id={1}>
        {renderPopoverBody(popoverEventHandlers)}
      </BootstrapPopover>
    )

    return (
      <OverlayTrigger ref={this.overlayRef} rootClose trigger={trigger} placement={placement} overlay={popoverElement}>
        {renderRoot(popoverEventHandlers)}
      </OverlayTrigger>
    )
  }
}

export default Popover
