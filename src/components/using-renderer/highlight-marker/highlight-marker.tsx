import React from 'react'
import classnames from 'classnames'

import {formatTime} from '../../../util'

const HighlightMarker = ({highlight, left, fullLength, seek, className}) => {
  return (
    <React.Fragment>
      <button
        className={classnames('highlightMarker', className)}
        style={{left: `${left}%`}}
        onClick={() => seek(highlight)}
      >
        {formatTime(highlight)}
      </button>
    </React.Fragment>
  )
}

export default HighlightMarker
