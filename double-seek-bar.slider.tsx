import React from 'react';
import PropTypes from 'prop-types'
import autobind from 'autobind-decorator';
import classnames from 'classnames';
import {
  noop,
} from 'react-player-controls/dist/utils.js';
import { Direction } from 'react-player-controls/dist/constants.js'
import RangeControlOverlay from 'react-player-controls/dist/components/RangeControlOverlay.js';

import './double-seek-bar.scss'

class Slider extends React.PureComponent {
  static propTypes = {
    direction: PropTypes.oneOf([Direction.HORIZONTAL, Direction.VERTICAL]),
    isEnabled: PropTypes.bool,
    onIntent: PropTypes.func,
    onChange: PropTypes.func,
    onChangeStart: PropTypes.func,
    onChangeEnd: PropTypes.func,
    children: PropTypes.node,
    className: PropTypes.string,
    style: PropTypes.object,
    overlayZIndex: PropTypes.number,
  }

  static defaultProps = {
    direction: Direction.HORIZONTAL,
    isEnabled: true,
    onIntent: noop,
    onChange: noop,
    onChangeStart: noop,
    onChangeEnd: noop,
    children: null,
    className: null,
    style: {},
    overlayZIndex: 10,
  }

  $el = null

  @autobind
  storeRef ($el) {
    this.$el = $el
  }

  @autobind
  handleIntent (intent) {
    if (this.props.isEnabled) {
      this.props.onIntent(intent)
    }
  }

  @autobind
  handleChange (value) {
    if (this.props.isEnabled) {
      this.props.onChange(value)
    }
  }

  @autobind
  handleChangeStart (value) {
    if (this.props.isEnabled) {
      this.props.onChangeStart(value)
    }
  }

  @autobind
  handleChangeEnd (value) {
    if (this.props.isEnabled) {
      this.props.onChangeEnd(value)
    }
  }

  render () {
    const {
      direction,
      children,
      className,
      style,
      overlayZIndex,
    } = this.props

    return (
      <div
        ref={this.storeRef}
        className={className}
        style={{
          position: 'relative',
          ...style,
        }}
      >
        {children}

        {/*
          TODO: Make it possible to render or extend this node yourself,
          so that these styles – the z-index property in particular – is
          not forced upon the component consumer.
        */}
        <RangeControlOverlay
          direction={direction}
          bounds={() => this.$el.getBoundingClientRect()}
          onIntent={this.handleIntent}
          onChange={this.handleChange}
          onChangeStart={this.handleChangeStart}
          onChangeEnd={this.handleChangeEnd}
          style={{
            position: 'absolute',
            top: 0,
            right: 0,
            bottom: 0,
            left: 0,
            zIndex: overlayZIndex,
          }}
        />
      </div>
    )
  }
}

export default Slider
